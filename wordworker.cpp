#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <vector>
#include <QQuickItem>
#include <QQuickView>
#include <QQmlContext>
#include "wordworker.h"

void WordWorker::addWord(std::string w){
    words[w]++;
}

void WordWorker::sortTop(int topNum){
    std::vector<std::pair<int, std::string>> sortWord;
    for(auto w: words)
        sortWord.push_back(std::pair<int, std::string>(w.second, w.first));
    std::sort(sortWord.begin(), sortWord.end());

    int i = 0;
    auto it = sortWord.end();
    while( it != sortWord.begin() && i < topNum){
        --it;
        i++;
        result[(*it).second]=(*it).first;
    }
}

bool WordWorker::loadFromFile(QString filename)
{
    QFile inputFile(filename);
    if(!inputFile.open(QIODevice::ReadOnly))
        return false;

    qDebug() << "File chosen: " + filename;

    finishWork = false;
    if(inputFile.size() == 0)
        return false;

    unsigned int start_time =  clock();
    words.clear();

    int oldProgressPercent  = 0;
    const int percentUpdate = 10;

    qDebug() << "Start file reading!";
    QTextStream fin(&inputFile);
    while(!fin.atEnd()) {
        QString line = inputFile.readLine();
        QStringList words = line.split(" ");    // TODO delete spec symbols and '\n'

        fileSize = words.size();

        foreach(QString word, words){
           addWord(word.toLower().toStdString());

           curProgress++;

           if(getProgressPercent() - oldProgressPercent > percentUpdate){
               emit progressTick();
               oldProgressPercent = getProgressPercent();
           }
        }
    }
    qDebug() << "File readed!";


    sortTop(TOP_NUM);
    unsigned int end_time = clock();
    unsigned int search_time = end_time - start_time;

    qDebug() << "search_time! " << search_time;

    inputFile.close();
    return true;
}

void WordWorker::work(){
    QString filename = QFileDialog::getOpenFileName(0, ("Open TXT File"), QDir::homePath(), ("TXT Files (*.txt)"));
    if(!loadFromFile(filename)){
        if(fileSize == 0)
            QMessageBox::information(0, "Error!", "File is empty!");
        else
            QMessageBox::information(0, "Error!", "File was not selected!");
        return;
    }

    result.clear();

    sortTop(TOP_NUM);

    finishWork = true;
    emit finishedWork();
    progressTick();

    for(auto w : result)
        qDebug() << QString::fromStdString(w.first) << ""  << w.second << '\n';

    emit setResult();
}

void WordWorker::progressTick() {
    emit progressChanged(getProgressPercent());
}

int WordWorker::getProgressPercent() const {
    if(finishWork)
        return 100;
    int percent = curProgress * 100/fileSize ;
    return percent > 100 ? 99 : percent;
}

void WordWorker::showAllWords() const{
    for(auto w : words)
        qDebug() << QString::fromStdString(w.first) << ""  << w.second << '\n';
}
