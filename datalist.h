#ifndef DATALIST_H
#define DATALIST_H

#include <QObject>
#include <QMap>

class DataList : public QObject
{
    Q_OBJECT
private:
    QMap<QString, int> dataList;
    QMap<QString, int>::iterator it;
public:
    Q_INVOKABLE QString     getWord() const { return it.key();            }
    Q_INVOKABLE int         getCount()const { return it.value();          }
    Q_INVOKABLE bool        isEnd()   const { return it == dataList.end();}
    Q_INVOKABLE void        next()          { it++;                       }
    Q_INVOKABLE void        clear()         { dataList.clear();           }

    DataList(){};
    void init(std::map<std::string, int> words);
};

#endif // DATALIST_H
