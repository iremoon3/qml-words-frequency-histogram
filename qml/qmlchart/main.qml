import QtQuick 2.2
import QtCharts 2.0
import QtQuick.Controls 2.15
import QtQuick.Window 2.2
import Qt.labs.platform 1.1

ChartView {
    width: 800
    height: 600
    title: "Top-15 words"
    antialiasing: true

    Connections {
        target: worker
        function onProgressChanged(progressPercent) {
            console.log("Get signal onProgressChanged");
            progressBar.indeterminate = false;
            progressBar.value = progressPercent/100;
        }
        function onProgressLoading() {
            console.log("Get signal onProgressLoading");
            progressBar.indeterminate = true;
        }
    }

    Connections {
        target: worker
        function onShowResult() {
            console.log("Get signal onShowResult");
            for (var i = 0; i < 15 &&  !list.isEnd(); i++)  {
                mySeries.append(list.getWord(), [list.getCount()]);
                list.next();
            }

            var min = 1e8, max = -1e8;
            for (var i = 0; i < mySeries.count; i++) {
                min = Math.min(min, mySeries.at(i).values.reduce(function(a,b) {
                    return Math.min(a, b);
                }));
                max = Math.max(max, mySeries.at(i).values.reduce(function(a,b) {
                    return Math.max(a, b);
                }));
            }
            mySeries.axisY.min = min - 1;
            mySeries.axisY.max = max;
        }
    }

    Rectangle{
        id: panel
        anchors.bottom: parent.bottom
        width: parent.width
        height: 30
        color: "#b1acac"

        Button {
            id: button
            x: 20
            y: 5
            width: 100
            height: 20
            text: qsTr("Open file")

            onClicked: {
                list.clear();
                mySeries.clear();
                thread.startWork();
            }
        }

        ProgressBar {
            id: progressBar
            x: 153
            y: 0
            width: 616
            height: 30
            value: 0


        }
    }

    BarSeries {
        id: mySeries
        axisX: BarCategoryAxis { categories: [" "] }
    }
}
