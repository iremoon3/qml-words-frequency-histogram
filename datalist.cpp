#include "datalist.h"

 void DataList::init(std::map<std::string, int> words){
    for(auto word: words)
        dataList.insert(QString::fromStdString(word.first), word.second);

    it = dataList.begin();
}
