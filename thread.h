#ifndef THREAD_H
#define THREAD_H

#include <QThread>
#include "wordworker.h"

class Thread : public QThread
{
    Q_OBJECT
public:
    explicit Thread(const QString & threadName);

    void run();

signals:
    void startWork() const;

private:
    QString name;
};

#endif // THREAD_H
