QT += charts qml quick widgets
CONFIG += c++11 qml_debug

SOURCES += \
    datalist.cpp \
    main.cpp \
    thread.cpp \
    wordworker.cpp

RESOURCES += \
    resources.qrc

DISTFILES += \
    qml/qmlchart/*

target.path = $$[QT_INSTALL_EXAMPLES]/charts/qmlchart
INSTALLS += target

HEADERS += \
    datalist.h \
    thread.h \
    wordworker.h
