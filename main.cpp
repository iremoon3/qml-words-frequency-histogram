#include <QtWidgets/QApplication>
#include <QtQuick/QQuickView>
#include <QtCore/QDir>
#include <QtQml/QQmlEngine>
#include <QQmlContext>
#include <QObject>
#include <QGraphicsObject>
#include <QQuickItem>
#include <QQuickView>
#include <QQmlContext>
#include "wordworker.h"
#include "thread.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QQuickView viewer;

    WordWorker wordWorker;
    viewer.rootContext()->setContextProperty("worker", &wordWorker);

#ifdef Q_OS_WIN
    QString extraImportPath(QStringLiteral("%1/../../../../%2"));
#else
    QString extraImportPath(QStringLiteral("%1/../../../%2"));
#endif

    viewer.engine()->addImportPath(extraImportPath.arg(QGuiApplication::applicationDirPath(),
                                      QString::fromLatin1("qml")));
    QObject::connect(viewer.engine(), &QQmlEngine::quit, &viewer, &QWindow::close);

    viewer.setTitle(QStringLiteral("Word frequency finder"));

    viewer.setSource(QUrl("qrc:/qml/qmlchart/main.qml"));
    viewer.setResizeMode(QQuickView::SizeRootObjectToView);
    viewer.show();

    DataList list;
    viewer.rootContext()->setContextProperty("list", &list);

    QObject::connect(
        &wordWorker, &WordWorker::setResult,
        [&]() {
            wordWorker.moveToThread(QApplication::instance()->thread());
            list.init(wordWorker.getResult());
            emit wordWorker.showResult();
    });

    Thread threadA("threadA");
    viewer.rootContext()->setContextProperty("thread", &threadA);

    QObject::connect(
        &threadA, &Thread::startWork,
        [&]() {
            wordWorker.moveToThread(&threadA);
            QObject::connect(&threadA, &Thread::started, &wordWorker, &WordWorker::work);

            threadA.start();
    });

    return app.exec();
}
