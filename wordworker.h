#ifndef WORDWORKER_H
#define WORDWORKER_H

#include <QtDebug>
#include <QString>
#include <QTimer>
#include <map>
#include <unordered_map>
#include <QQuickView>
#include "datalist.h"

class WordWorker : public QObject
{
    Q_OBJECT
private:
    const int TOP_NUM = 15;

    int fileSize = 0;
    int curProgress = 0;

    bool finishWork = false;

    std::unordered_map<std::string, int> words;
    std::map<std::string, int> result;

    void addWord(std::string w);

    void sortTop(int topNum);

    bool loadFromFile(QString filename);

public:

    int getProgressPercent() const;

    void showAllWords() const;

    std::map<std::string, int> getResult() const { return result;}

    WordWorker(){};

public slots:
    void progressTick();
    void work();

signals:
    void progressChanged (QVariant progressPercent) const;
    void setResult       (                        ) const;
    void showResult      (                        ) const;
    void finishedWork    (                        ) const;
    void progressLoading (                        ) const;
};

#endif // WORDWORKER_H
